package kz.aitu.week8.sort;

public class Insertion {
    public void sort(int[] arr){
        int j, temp;
        for(int i = 1; i < arr.length; i++){
            temp = arr[i];
            j = i - 1;
            while(j >= 0 && temp < arr[j]){
                arr[j+1] = arr[j];
                j--;
            }
            arr[j+1] = temp;
        }
    }
}
