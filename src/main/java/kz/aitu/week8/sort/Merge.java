package kz.aitu.week8.sort;

public class Merge {
    public void sort(int[] arr){
        int l = arr.length;
        mergeSort(arr, l);
    }
    public static void mergeSort(int[] a, int l) {
        if (l <= 1) {
            return;
        }
        int mid = l / 2;
        int[] left = new int[mid];
        int[] right = new int[l - mid];
        for (int i = 0; i < mid; i++) {
            left[i] = a[i];
        }
        for (int i = mid; i < l; i++) {
            right[i - mid] = a[i];
        }
        mergeSort(left, mid);
        mergeSort(right, l - mid);
        merge(a, left, right, mid, l - mid);
    }

    public static void merge(int[] arr, int[] left, int[] right, int l, int r) {
        int i = 0, j = 0, k = 0;
        while (i < l && j < r) {
            if (left[i] <= right[j]) {
                arr[k++] = left[i++];
            }
            else {
                arr[k++] = right[j++];
            }
        }
        while (i < l) {
            arr[k++] = left[i++];
        }
        while (j < r) {
            arr[k++] = right[j++];
        }
    }
}
