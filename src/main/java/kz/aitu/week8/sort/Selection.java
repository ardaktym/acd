package kz.aitu.week8.sort;

public class Selection {
    public void sort(int[] arr){
        int minIndex, minNumber = 0;
        int temp = 0;
        for(int i = 0; i < arr.length; i++){
            minNumber = arr[i];
            minIndex = i;
            for(int j = i; j < arr.length; j++) {
                if (arr[j] < minNumber) {
                    minNumber = arr[j];
                    minIndex = j;
                }
            }
            if(minNumber < arr[i]){
                temp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = temp;
            }
        }
    }
}
