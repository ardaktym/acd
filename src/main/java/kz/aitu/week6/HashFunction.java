package kz.aitu.week6;

import java.util.Arrays;

public class HashFunction {
    String[] theArray;
    int arraySize;
    int itemsInArray = 0;

    public static void main(String[] args) {
        HashFunction theFunc = new HashFunction(30);
        String[] elemntsToAdd2 = {
                "101", "102", "103", "104", "105", "106", "107", "108", "109", "110",
                "111", "112", "113", "114", "115", "116", "117", "118", "119", "120",
                "129", "128", "127", "126", "125", "124", "123", "122", "121", "130",
        };
        theFunc.hashFunction2(elemntsToAdd2, theFunc.theArray);
        theFunc.findKey("121");
        theFunc.displayTheStack();
    }

    public void hashFunction1(String[] stringsForArray, String[] theArray) {
        for (int i = 0; i < stringsForArray.length; i++) {
            String newElementVal = stringsForArray[i];
            theArray[Integer.parseInt(newElementVal)] = newElementVal;
        }
    }

    public void hashFunction2(String[] stringsForArray, String[] theArray) {
        for (int i = 0; i < stringsForArray.length; i++) {
            String newElementVal = stringsForArray[i];
            int arrayIndex = Integer.parseInt(newElementVal) % 29;
            System.out.println(arrayIndex + " " + newElementVal);
            while (theArray[arrayIndex] != "-1") {
                arrayIndex++;
                System.out.println(arrayIndex);
                arrayIndex %= arraySize;
            }
            theArray[arrayIndex] = newElementVal;
        }
    }

    public String findKey(String key) {
        int arrayIndexHash = Integer.parseInt(key) % 29;
        while (theArray[arrayIndexHash] != "-1") {
            if (theArray[arrayIndexHash] == key) {
                System.out.println(key + " " + arrayIndexHash);
                return theArray[arrayIndexHash];
            }
            ++arrayIndexHash;
            arrayIndexHash %= arraySize;
        }
        return null;
    }

    HashFunction(int size) {
        arraySize = size;
        theArray = new String[size];
        Arrays.fill(theArray, "-1");
    }

    public void displayTheStack() {
        int increment = 0;
        for (int m = 0; m < 3; m++) {
            increment += 10;
            for (int n = 0; n < 71; n++) {
                System.out.println();
            }
            for (int n = increment - 10; n < increment; n++) {
                System.out.format("| %3s " + " ", n);
            }
            System.out.println("|");
            for (int n = 0; n < 71; n++)
                System.out.print("-");
            System.out.println();
            for (int n = increment - 10; n < increment; n++) {
                if (theArray[n].equals("-1"))
                    System.out.print("|      ");
                else
                    System.out.print(String.format("| %3s " + " ", theArray[n]));
            }
            System.out.println("|");
            for (int n = 0; n < 71; n++)
                System.out.print("-");
            System.out.println();
        }
    }
}
