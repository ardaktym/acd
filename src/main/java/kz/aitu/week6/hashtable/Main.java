package kz.aitu.week6.hashtable;

public class Main {
    public static void main(String[] args) {
        Hashtable hashtable = new Hashtable(5);
        hashtable.add("0","zero");
        hashtable.add("1","one");
        hashtable.add("2","two");
        hashtable.add("3","three");
        hashtable.add("4","four");
        hashtable.print();
        System.out.println();
        hashtable.delete("3");
        hashtable.print();
    }

}
