package kz.aitu.week6.hashtable;

public class Hashtable {
    int size;
    Node arr[];
    Node tail;
    public Hashtable(int size){
        this.tail = null;
        this.size = size;
        arr = new Node[size];
    }
    public void add(String key,String data){
        int code = key.hashCode();
        int index=code % size;
        Node node=new Node(key, data);
        if (arr[index]==null){
            arr[index]=node;
            tail=node;
        }
        else {
            tail.next=node;
            tail=node;
        }
    }
    public void print(){
        for (int i=0;i<size;i++){
            Node temp=arr[i];
            while (temp!=null){
                System.out.print(temp.data + " ");
                temp=temp.next;
            }
        }
    }
    public void delete(String key){
        int code = key.hashCode();
        int index = code % size;
        arr[index] = null;
    }
}
