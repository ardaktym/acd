package kz.aitu.week6.hashtable;

public class Node {
    String data;
    String key;
    Node next;
    public Node (String key, String data){
        this.data = data;
        this.key = key;
    }
    public void setData(String data) {
        this.data = data;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext() {
        return next;
    }

    public String getData() {
        return data;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
