package kz.aitu.week6.bst;

public class Main {
    public static void main(String[] args) {
        BSTree bsTree = new BSTree();

        bsTree.insert(1000, "A");
        bsTree.insert(2000, "B");
        bsTree.insert(500, "C");
        bsTree.insert(1500, "D");
        bsTree.insert(750, "E");
        bsTree.insert(250, "F");
        bsTree.insert(625, "G");
        bsTree.insert(1250, "H");
        bsTree.insert(875, "I");
        bsTree.insert(810, "Z");


        System.out.println(bsTree.height(bsTree.root));
        bsTree.printAllAscending(bsTree.root);
        System.out.println();
        bsTree.printAll(bsTree.root);
        bsTree.deleteNode(bsTree.root, 1000);
        System.out.println();
        System.out.println("delete A");
        bsTree.printAllAscending(bsTree.root);
        System.out.println();
        bsTree.printAll(bsTree.root);
        System.out.println();
        bsTree.deleteNode(bsTree.root, 750);
        System.out.println("delete E");
        bsTree.printAllAscending(bsTree.root);
        System.out.println();
        bsTree.printAll(bsTree.root);

    }

}
