package kz.aitu.week6.bst;

public class BSTree {
    Node root;

    public BSTree() {
        this.root = null;
    }
    //inserting values
    public void insert(Integer key, String value) {

        //WRITE YOUR CODE HERE
        Node newNode = new Node(key,value);
        if(root == null){
            root = newNode;
            return;
        }
        Node current = root;
        Node parent = null;
        while(true){
            parent = current;
            if(key < current.getKey()){
                current = current.getLeft();
                if(current == null){
                    parent.left = newNode;
                    return;
                }
            }else {
                current = current.getRight();
                if(current == null){
                    parent.right = newNode;
                    return;
                }
            }
        }
    }

    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }

    public String findWithoutRecursion(Integer key) {
        Node current = root;
        while(current!=null) {
            if(key > current.getKey()) current = current.getRight();
            else if(key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }

    public void printAllAscending(Node root) {
        if(root != null){
            printAllAscending(root.left);
            System.out.print(" " + root.value);
            printAllAscending(root.right);
        }
    }

    public void printAll(Node root) {
        int height = height(root);
        for( int i = 0; i < height; i++){
            printAllHelper(root, i);
        }
    }

    public int height(Node root){
        if(root == null){
            return 1;
        }
        else {
            int lHeight = height(root.left);
            int rHeight = height(root.right);
            if(lHeight > rHeight){
                return lHeight+1;
            }else{
                return rHeight+1;
            }
        }
    }

    public void printAllHelper(Node root, int level){
        if(root == null){
            return;
        }
        if(level == 1){
            System.out.print(" " + root.value);
        }
        else if(level > 1){
            printAllHelper(root.getLeft(), level -1);
            printAllHelper(root.getRight(), level -1);
        }
    }

    public Node deleteNode(Node root, int value){
        if(root == null) return null;
        if(value < root.key){
            root.setLeft(deleteNode(root.getLeft(), value));
        }else if(value > root.getKey()){
            root.setRight(deleteNode(root.getRight(), value));
        }
        else{
            if(root.getLeft() == null && root.getRight() == null){
                return null;
            }else if(root.getLeft() == null){
                return root.getRight();
            }else if(root.getRight() == null){
                return root.getLeft();
            }
            else {
                Integer minValue = minValue(root.getRight());
                String str = rootsolve(root.getRight());
                root.setValue(str);
                root.setKey(minValue);
                root.setRight(deleteNode(root.getRight(), minValue));
            }
        }
        return root;
    }
    int minValue(Node node) {
        if((node.getLeft() != null)){
            return minValue(node.getLeft());
        }
        return node.getKey();
    }
    String rootsolve(Node node){
        if(node.getLeft() !=null){
            return rootsolve(node.getLeft());
        }
        return node.getValue();
    }

   /* public boolean delete(Node root, int value){
        if(root == null) return true;
        switch (root.value == findWithoutRecursion(value))
    }*/
}
