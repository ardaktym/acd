package kz.aitu.week6.test;

public class BinarySearchTreeImpl {
    private BstNode root;

    public boolean isEmpty() {

        return (this.root == null);
    }

    public BstNode getRoot() {
        return this.root;
    }

    public void insert(Integer data) {

        System.out.print("[input: "+data+"]");
        if(root == null) {
            this.root = new BstNode(data);
            System.out.println(" -> inserted: "+data);
            return;
        }

        insertNode(this.root, data);
        System.out.print(" -> inserted: "+data);
        System.out.println();
    }

    private BstNode insertNode(BstNode root, Integer data) {

        BstNode tmpNode = null;
        System.out.print(" ->"+root.getData());
        if(root.getData() >= data) {
            System.out.print(" [L]");
            if(root.getLeft() == null) {
                root.setLeft(new BstNode(data));
                return root.getLeft();
            } else {
                tmpNode = root.getLeft();
            }
        } else {
            System.out.print(" [R]");
            if(root.getRight() == null) {
                root.setRight(new BstNode(data));
                return root.getRight();
            } else {
                tmpNode = root.getRight();
            }
        }

        return insertNode(tmpNode, data);
    }

    public void delete(Integer data) {

        deleteNode(this.root, data);
    }

    private BstNode deleteNode(BstNode root, Integer data) {

        if(root == null) return root;

        if(data < root.getData()) {
            root.setLeft(deleteNode(root.getLeft(), data));
        } else if(data > root.getData()) {
            root.setRight(deleteNode(root.getRight(), data));
        } else {
            // node with no leaf nodes
            if(root.getLeft() == null && root.getRight() == null) {
                System.out.println("deleting "+data);
                return null;
            } else if(root.getLeft() == null) {
                // node with one node (no left node)
                System.out.println("deleting "+data);
                return root.getRight();
            } else if(root.getRight() == null) {
                // node with one node (no right node)
                System.out.println("deleting "+data);
                return root.getLeft();
            } else {
                // nodes with two nodes
                // search for min number in right sub tree
                Integer minValue = minValue(root.getRight());
                root.setData(minValue);
                root.setRight(deleteNode(root.getRight(), minValue));
                System.out.println("deleting 8"+data);
            }
        }

        return root;
    }

    private Integer minValue(BstNode node) {

        if(node.getLeft() != null) {
            return minValue(node.getLeft());
        }
        return node.getData();
    }

    public void inOrderTraversal() {
        doInOrder(this.root);
    }

    private void doInOrder(BstNode root) {

        if(root == null) return;
        doInOrder(root.getLeft());
        System.out.print(root.getData()+" ");
        doInOrder(root.getRight());
    }

    public static void main(String a[]) {

        BinarySearchTreeImpl bst = new BinarySearchTreeImpl();
        bst.insert(1000);
        bst.insert(2000);
        bst.insert(500);
        bst.insert(1500);
        bst.insert(750);
        bst.insert(250);
        bst.insert(625);
        bst.insert(1250);
        bst.insert(875);
        bst.insert(810);
        System.out.println("-------------------");
        System.out.println("In Order Traversal");
        System.out.println(bst.minValue(bst.root));
        bst.inOrderTraversal();
        System.out.println();
        bst.delete(1000);
        bst.inOrderTraversal();
        System.out.println();
        bst.delete(1250);
        bst.inOrderTraversal();
    }
}
