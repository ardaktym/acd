package kz.aitu.week6.tree;

public class Node {
    Integer key;
    String value;
    kz.aitu.week6.bst.Node left;
    kz.aitu.week6.bst.Node right;

    public Node(Integer key, String value) {
        this.key = key;
        this.value = value;
        this.left = null;
        this.right = null;
    }


    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public kz.aitu.week6.bst.Node getLeft() {
        return left;
    }

    public void setLeft(kz.aitu.week6.bst.Node left) {
        this.left = left;
    }

    public kz.aitu.week6.bst.Node getRight() {
        return right;
    }

    public void setRight(kz.aitu.week6.bst.Node right) {
        this.right = right;
    }

}
