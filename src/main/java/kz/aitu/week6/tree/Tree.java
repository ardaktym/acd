package kz.aitu.week6.tree;

public class Tree {
    Node root;
    public Tree(){
        this.root = null;
    }

  public void insert(Integer key, String value) {
   /*   //WRITE YOUR CODE HERE
        Node newNode = new Node(key,value);
        if(root == null){
            root = newNode;
            return;
        }
        Node current = root;
        Node parent = null;
        while(true){
            parent = current;
            if(key < current.getKey()){
                current = current.getKey();
                if(current == null){
                    parent.left = newNode;
                    return;
                }
            }else {
                current = current.getRight();
                if(current == null){
                    parent.right = newNode;
                    return;
                }
            }
        }
    }
    public void print(Node root){
        if (root != null){
            print(root.left);
            System.out.print(" " + root.data);
            print(root.right);
        }
    }
    public void reverse(Node root){
        if(root != null){
            reverse(root.right);
            System.out.print(" "+ root.data);
            reverse(root.left);
        }
    }
    public boolean find(int value) {
        Node currentNode = root;
        while (currentNode != null) {
            if (value == currentNode.data) {
                return true;
            } else if (value > currentNode.data) {
                currentNode = currentNode.right;
            } else {
                currentNode = currentNode.left;
            }
        }
        return false;
    }


    public Node delete(Node root, int value){
        if(root == null)return null;
        if(value < root.data){
            root.setLeft(delete(root.getLeft(), value));
        }else if(value > root.getData()){
            root.setRight(delete(root.getRight(), value));
        }
        else{
            if(root.getLeft() == null && root.getRight() == null){
                return null;
            }else if(root.getLeft() == null){
                return root.getRight();
            }else if(root.getRight() == null){
                return root.getLeft();
            }
            else {
                int minValue = minValue(root.getRight());
                root.setData(minValue);
                root.setRight(delete(root.getRight(), minValue));
            }
        }
        return root;
    }

    int minValue(Node node) {

        if (node.getLeft() != null) {
            return minValue(node.getLeft());
        }
        return node.getData();
    }

    public void printAll(Node root){
        int height = height(root);
        for(int i = 0; i < height; i++){
            printall1(root, i);
        }
    }

    public int height( Node root){
        if( root == null){
            return 1;
        }
        else{
            int Lheight = height(root.left);
            int Rheight = height(root.right);
            if(Lheight > Rheight){
                return Lheight++;
            }
            else {
                return Rheight++;
            }
        }
    }

    public void printall1(Node root, int level){
        if(root == null){
            return;
        }
        if(level == 1){
            System.out.print(" " + root.data);
        }
        else if(level > 1){
            printall1(root.getLeft(), level-1);
            printall1(root.getRight(), level-1);
        }
    }*/
  }

}
