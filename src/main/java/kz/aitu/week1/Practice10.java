package kz.aitu.week1;
import java.util.Scanner;
public class Practice10 {
    static int gcd(int a,int b)
    {
        if(b != 0)
            return gcd(b, a % b);
        return a;
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        System.out.println(gcd(a,b));
    }

}
