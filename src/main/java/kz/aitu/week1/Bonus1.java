package kz.aitu.week1;
import java.util.Scanner;
public class Bonus1 {
    static int distinct(int a[], int n) {
        int counter = 1;
        for (int i = 1; i < n; i++) {
            int j = 0;
            for (; j < i; j++)
                if (a[i] == a[j])
                    break;
            if (i == j)
                counter++;
        }
        return counter;
    }

    public static void main(String[] args) {
        int[] a;
        int n;
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        System.out.println(distinct(a, n));
    }
}
