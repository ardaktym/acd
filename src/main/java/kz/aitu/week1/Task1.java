package kz.aitu.week1;

import java.util.Scanner;

public class Task1 {

    static String printNumbers(int n){
        if(n==1)
            return "1 ";
        return printNumbers(n-1) + n + " ";
    }
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println(printNumbers(n));
    }
}
