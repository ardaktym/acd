package kz.aitu.week1;
import java.util.Scanner;
public class Practice8 {
    static boolean checker(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (Character.isDigit(s.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str;
        str = scan.nextLine();
        if (checker(str))
            System.out.println("Yes");
        System.out.println("No");
    }
}