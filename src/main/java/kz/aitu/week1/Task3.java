package kz.aitu.week1;

import java.util.Scanner;

public class Task3{
    public static void main(String[] args){
        secondMax(0,0);
    }

    public static void secondMax(int max1, int max2){
        Scanner input = new Scanner(System.in);
        int n;
        n = input.nextInt();

        if(n != 0){
            if(max1 < n){
                secondMax(n, max1);
            }else if(max2 < n){
                secondMax(max1, n);
            }else secondMax(max1, max2);
        }else{
            System.out.println(max2);
        }
    }

}

