package kz.aitu.week1;

import java.util.Scanner;
public class Practice6 {
    static int function(int a,int n)
    {
        int answer=1;
        for(int i=1;i<=n;i++)
        {
            answer=answer*a;
        }
        return answer;
    }
    /* or
    static int function(int a,int n)
    {
       if(n==0)
             return 1;
        return a*function(a,n-1);
    }
   */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int n = scan.nextInt();
        System.out.println(function(a,n));
    }


    public static class Main {

        public static void main(String[] args) {
            labirint labirint = new labirint();
            labirint.run();
        }
    }
}
