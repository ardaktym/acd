package kz.aitu.week1;
import java.util.Scanner;
public class Practice4 {
    static int findFactorial(int n)
    {
        if(n==0 || n==1)
            return 1;
        else
            return n*findFactorial(n-1);

    }
    public static void main(String args[])
    {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println(findFactorial(num));
    }

}
