package kz.aitu.week1;
import java.util.Scanner;
public class Practice9 {
    static int binomialCoefficient(int n,int k)
    {
        if(k == 0 || k == n)
            return 1;
        return binomialCoefficient(n-1,k-1)+ binomialCoefficient(n-1,k);
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int k = scan.nextInt();
        System.out.println(binomialCoefficient(n,k));
    }


}
