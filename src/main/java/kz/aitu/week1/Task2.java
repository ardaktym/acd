package kz.aitu.week1;
import java.util.Scanner;

public class Task2 {

    public static int MaximumValue()
    {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        if(n==0)
            return 0;
        int maximum = MaximumValue();
        if(maximum>n)
            return maximum;
        return n;
    }

    public static void main(String args[])
    {
        System.out.println(MaximumValue());
    }
}
