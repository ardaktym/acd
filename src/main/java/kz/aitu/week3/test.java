package kz.aitu.week3;

import java.util.Scanner;

public class test {
    static class SinglyLinkedListNode{
        public int data;
        public SinglyLinkedListNode next;
        public SinglyLinkedListNode(int nodeData){
            this.data = nodeData;
            this.next = null;
        }
    }
    static class SinglyLinkedList {
        public SinglyLinkedListNode head;
        public SinglyLinkedListNode tail;
        public SinglyLinkedList(){
            this.head = null;
            this.tail = null;
        }
        public  void insertNode(int nodeData){
            SinglyLinkedListNode node = new SinglyLinkedListNode(nodeData);
            if(this.head == null){
                this.head = node;
            }
            else{
                this.tail.next = node;
            }
            this.tail = node;
        }
    }
    /*just print elements of linked list
    static void printLinkedList(SinglyLinkedListNode head) {
        if (head == null) return;
        else {
            while (head != null) {
                System.out.println(head.data);
                head = head.next;
            }
        }
    }*/
    static SinglyLinkedListNode insertNodeTail(SinglyLinkedListNode head, int data){
        SinglyLinkedListNode new_node = new SinglyLinkedListNode(data);
        if(head == null){
            head = new_node;
            return head;
        }
        SinglyLinkedListNode current_node = head;
        while(current_node.next != null){
            current_node = current_node.next;
        }
        current_node.next = new_node;
        return head;
    }
    public static void main(String[] args) {
        SinglyLinkedList list = new SinglyLinkedList();
        Scanner scanner = new Scanner(System.in);
        int counter = scanner.nextInt();
        for(int i = 0; i < counter; i++){
            int a = scanner.nextInt();
            SinglyLinkedListNode list_head = insertNodeTail(list.head, a);
            list.head = list_head;

        }

        //printLinkedList(list.head);
    }



}
