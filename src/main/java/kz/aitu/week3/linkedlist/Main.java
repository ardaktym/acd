package kz.aitu.week3.linkedlist;

public class Main {
    public static void main(String[] args) {
        Node list1 = new Node();
        Node list2 = new Node();
        Node list3 = new Node();
        Node list4 = new Node();
        Node list5 = new Node();
        Node list6 = new Node();
        list1.setData("one");
        list2.setData("two");
        list3.setData("three");
        list4.setData("four");
        list5.setData("five");
        list6.setData("six");

        list1.setNext(list1);
        list2.setNext(list3);
        list3.setNext(list4);
        list4.setNext(list5);
        list5.setNext(list6);
        list6.setNext(null);

        Node current = list1;
        int counter = 0;
        while(current != null){
            current= current.getNext();
            counter++;
        }

    }
}
