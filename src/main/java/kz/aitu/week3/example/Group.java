package kz.aitu.week3.example;

public class Group {
    private long id;
    private String name;
    private int number;
    private Student monitor;
    private Student[] students;

    public Group(){
        monitor = new Student();
        students = new Student[20];
        number = 0;
    }

    public long getId(){
        return id;
    }

    public void setId(long id){
        this.id= id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Student getMonitor() {
        return monitor;
    }

    public void setMonitor(Student monitor) {
        this.monitor = monitor;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    public void addStudent(Student student){
        students[number]=student;
        number++;
    }

    public double avgGroup(){
        int n = getNumber();
        double sum = 0;
        for (int i = 0;i<n; i++){
            sum += getStudents()[i].getAge();
        }
        return sum/n;
    }
}
