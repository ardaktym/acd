package kz.aitu.week3.example;

public class Main {
    public static void main(String[] args) {
        Group cs1902 = new Group();
        cs1902.setId(1);
        cs1902.setName("cybersecurity");

        Student st = new Student();
        st.setId(10);
        st.setName("Alisher");
        st.setAge(19);

        cs1902.setMonitor(st);

        Student a1 = new Student();
        a1.setAge(20);
        a1.setName("Daulet");

        Student a2 = new Student();
        a2.setName("Aslan");
        a2.setAge(19);

        cs1902.addStudent(st);
        cs1902.addStudent(a1);
        cs1902.addStudent(a2);

        System.out.println(cs1902.avgGroup());
    }
}
