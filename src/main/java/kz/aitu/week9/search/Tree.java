package kz.aitu.week9.search;

import kz.aitu.week9.search.Node;
import kz.aitu.week9.search.Queue;
import kz.aitu.week9.search.Stack;

public class Tree {
    private Node root;

    public void put(int key, String value) {
        root = put(root, key, value);
    }

    private Node put(Node node, int key, String value) {
        if (node == null) return new Node(key, value);
        int cmp = key - (node.getKey());
        if (cmp < 0)
            node.setLeft(put(node.getLeft(), key, value));
        else if (cmp > 0)
            node.setRight(put(node.getRight(), key, value));
        else if (cmp == 0)
            node.setValue(value);
        return node;
    }

    public void bfs() {
        Queue queue = new Queue();
        queue.push(root.getKey());
        print(root, queue);
    }

    private void print(Node root, Queue queue) {
        while (queue.isEmpty() == false) {
            Node current = root;
            int
                    c = queuePrint(queue);
            while (current != null) {
                if (c < current.getKey())
                    current = current.getLeft();
                else if (c > current.getKey())
                    current = current.getRight();
                else break;
            }
            if (current.getLeft() != null)
                queue.push(current.getLeft().getKey());
            if (current.getRight() != null)
                queue.push(current.getRight().getKey());
        }
    }

    private int queuePrint(Queue queue) {
        int rot = queue.pop();
        Node current = root;
        while (current != null) {
            if (rot < current.getKey())
                current = current.getLeft();
            else if (rot > current.getKey())
                current = current.getRight();
            else {
                System.out.print(current.getValue() + " ");
                break;
            }
        }
        return rot;
    }

    public void dfs() {
        Stack stack = new Stack();
        stack.push(root.getKey());
        print2(root, stack);
    }

    public void print2(Node root, Stack stack){
        while(stack.empty() == false){
            Node current = root;
            int c = stackPrint(stack);
            while ((current != null)){
                if(c < current.getKey())
                    current = current.getLeft();
                else if(c > current.getKey())
                    current = current.getRight();
                else
                    break;
            }
            if(current.getLeft() != null){
                stack.push(current.getLeft().getKey());
            }
            if(current.getRight() != null){
                stack.push(current.getRight().getKey());
            }
        }
    }
    private int stackPrint(Stack stack){
        int rot = stack.pop();
        Node current = root;
        while(current != null){
            if(rot < current.getKey())
                current = current.getLeft();
            else if( rot > current.getKey())
                current = current.getRight();
            else{
                System.out.print(current.getValue() + " ");
                break;
            }
        }
        return rot;
    }

}