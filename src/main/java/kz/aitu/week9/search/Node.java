package kz.aitu.week9.search;

public class Node {
    private int key;
    private String value;
    private Node left;
    private Node right;
    public Node (int key, String value){
        this.key = key;
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
