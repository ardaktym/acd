package kz.aitu.week9.search;

import kz.aitu.week9.search.NodeQueue;

public class Queue {
    private NodeQueue head;
    private NodeQueue tail;
    public void push(int key){
        NodeQueue newNode = new NodeQueue(key);
        if(head == null){
            head = newNode;
            tail = newNode;
        }else{
            tail.setNext(newNode);
            tail = newNode;
        }
    }
    public void print(){
        NodeQueue  current = head;
        while(current != null){
            System.out.print(current + " ");
            current = current.getNext();
        }
    }
    public int pop(){
        NodeQueue current = head;
        head = head.getNext();
        return current.getData();
    }
    public boolean isEmpty(){
        return head == null;
    }
}
