package kz.aitu.week9.search;

public class Main {
    public static void main(String[] args) {
        Tree tree = new Tree();
        tree.put(1000,"A");
        tree.put(2000, "B");
        tree.put(500, "C");
        tree.put(1500, "D");
        tree.put(750, "E");
        tree.put(250, "F");
        tree.put(625, "G");
        tree.put(1250, "H");
        tree.put(875, "I");
        tree.put(810, "Z");
        tree.bfs();
        System.out.println();
        tree.dfs();




    }
}
