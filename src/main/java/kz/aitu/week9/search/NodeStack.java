package kz.aitu.week9.search;

public class NodeStack {
    private int value;
    private NodeStack nextTop;
    public NodeStack(int value){
        this.value = value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public NodeStack getNextTop() {
        return nextTop;
    }

    public void setNextTop(NodeStack nextTop) {
        this.nextTop = nextTop;
    }
}
