package kz.aitu.week9.search;

import kz.aitu.week9.search.NodeStack;

public class Stack {
    private NodeStack top;
    private int size = 0;

    public void setTop(NodeStack top) {
        this.top = top;
    }

    public NodeStack getTop() {
        return top;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    public void push(int value){
        NodeStack newNode = new NodeStack(value);
        if(top == null){
            top = newNode;
            setSize(+1);
        }
        else{
            newNode.setNextTop(top);
            top = newNode;
            setSize(+1);
        }
    }
    public int pop(){
        NodeStack c = top;
        top = top.getNextTop();
        setSize(-1);
        return c.getValue();
    }
    public boolean empty(){
        if(top == null)
            return true;
        else
            return false;
    }


}
