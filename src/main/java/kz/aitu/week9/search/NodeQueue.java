package kz.aitu.week9.search;

public class NodeQueue {
    private int data;
    private NodeQueue next;

    public NodeQueue(int data){
        this.data = data;
        this.next = null;
    }
    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setNext(NodeQueue next) {
        this.next = next;
    }

    public NodeQueue getNext() {
        return next;
    }

}
