package kz.aitu.week9.graph;

public class Hashtable<Key, Value> {
    private Vertex<Key, Value> table[];
    private int size = 11;
    public Hashtable(){
        table = new Vertex[11];
    }
    public boolean containsKey(Key key){
        if(table[key.hashCode() % size] != null){
            Vertex current = table[key.hashCode() % size];
            while(current != null){
                if(current.getKey() == key){
                    return true;
                }
                current = current.getNext();
            }
        }
        return false;
    }
    public Vertex get(Key key){
        if(table[key.hashCode() % size] != null){
            Vertex current = table[key.hashCode() % size];
            while(current != null){
                if(current.getKey() == key){
                    return current;
                }
                current = current.getNext();
            }
        }
        return null;
    }
    public  void put(Key key, Vertex vertex){
    if(table[key.hashCode() % size] == null){
        table[key.hashCode() % size] = vertex;
    }
    else {
            Vertex current = table[key.hashCode() % size];
            while (current != null) {
                if (current.getKey() == vertex.getKey()) {
                    current.setValue(vertex.getValue());
                    break;
                }
                current = current.getNext();
            }
            if (current == null) {
                vertex.setNext(table[key.hashCode() % size]);
                table[key.hashCode() % size] = vertex;
            }
        }
    }
    public void printAll() {
        for (int i = 0; i < size; i++) {
            Vertex current = table[i];
            while (current != null) {
                System.out.print(current.getValue() + " ");
                current = current.getNext();
            }
        }
    }
    public int countVertex() {
        int count = 0;
        for(int i = 0; i < size; i++) {
            Vertex current = table[i];
            while (current != null) {
                count++;
                current = current.getNext();
            }
        }
        return count;
    }

}

