package kz.aitu.week9.graph;

import java.sql.SQLOutput;

public class LinkedList<Key> {
    private Vertex first;
    private Vertex last;
    public LinkedList(){
        first = null;
        last = null;
    }
    public boolean contains(Vertex vertex){
        if(first != null){
            Vertex current = first;
            while(current != null){
                if(current == vertex){
                    return true;
                }
                current = current.getNextforlist();
            }
            return false;
        }
        return false;
    }
    public void add(Vertex vertex){
        if(first == null){
            first = vertex;
            last = vertex;
        }else {
            last.setNextforlist(vertex);
            last = vertex;
        }
    }

    public void connectedWith (){
        int count = 0;
        Vertex current = first;
        while(current != null){
            System.out.print(current.getKey() + " " + current.getValue());
            System.out.println();
            current = current.getNextforlist();
            count++;
        }
        System.out.println(count);
    }
    public boolean isEmpty(){
        return first == null;
    }
    public boolean isConnected(Key key2){
        Vertex current = first;
        while(current != null){
            if(current == key2){
                return true;
            }
            current = current.getNext();
        }
        return false;
    }

}
