package kz.aitu.week9.graph;

public class Graph<Key, Value> {
    private boolean biDirection;
    private Hashtable<Key,Value> vertexTable = new Hashtable<Key, Value>();
    public void addVertex(Key key, Value value){
        Vertex vertex = new Vertex<Key, Value>(key, value);
        if(vertexTable.containsKey(key))
            return;
        else
            vertexTable.put(key, vertex);
    }
    public void addEdge(Key key1, Key key2, boolean biDirection){
        Vertex vertexA = vertexTable.get(key1);
        Vertex vertexB = vertexTable.get(key2);
        vertexA.addEdge(vertexB);
        if(biDirection)
            vertexB.addEdge(vertexA);
    }
    public void connectedWith(Key key){
        Vertex vertex = vertexTable.get(key);
        vertex.connectedWith();
    }
    public boolean isConnected(Key key1, Key key2){
        Vertex vertexA = vertexTable.get(key1);
        Vertex vertexB = vertexTable.get(key2);
        return vertexA.isConnected(vertexB);
    }
    public void printAll(){
        vertexTable.printAll();
        System.out.println();
    }
    public void countVertex(){
        System.out.println(vertexTable.countVertex());
    }
}
