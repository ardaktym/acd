package kz.aitu.week9.graph;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph<Integer, String>();
        graph.addVertex( 5, "Arman");
        graph.addVertex(7, " Zharylkasyn");
        graph.addVertex(1, "Dimash");
        graph.addVertex(3, "Azamat");
        graph.addEdge(3,1, true);
        graph.addEdge(5, 7, true);
        graph.addEdge(3, 5,false);
        graph.connectedWith(5);
        graph.connectedWith(3);
        System.out.println(graph.isConnected(3,5));
        graph.printAll();
        graph.countVertex();
    }
}
