package kz.aitu.week9.graph;

public class Vertex<Key, Value> {
    private Key key;
    private Value value;
    private LinkedList<Key> edges;
    private Vertex next;
    private Vertex nextforlist;
    public Vertex(Key key, Value value){
        this.key = key;
        this.value = value;
        edges = new LinkedList<>();
    }
    public void addEdge(Vertex vertex){
        if(!edges.contains(vertex)) edges.add(vertex);
    }
    public void connectedWith(){
        if(!edges.isEmpty()){
            edges.connectedWith();
        } else{
            System.out.println(key);
        }
    }
    public boolean isConnected(Key key2){
        if(edges.isEmpty()){
            return false;
        }
        return edges.isConnected(key2);
    }
    public boolean equals(Vertex o){
        if(o == null) return false;
        if(this.key == o.getKey())
            return true;
        return false;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public void setNext(Vertex next) {
        this.next = next;
    }

    public Key getKey() {
        return key;
    }

    public LinkedList<Key> getEdges() {
        return edges;
    }

    public Value getValue() {
        return value;
    }

    public Vertex getNext() {
        return next;
    }

    public Vertex getNextforlist() {
        return nextforlist;
    }

    public void setEdges(LinkedList<Key> edges) {
        this.edges = edges;
    }

    public void setNextforlist(Vertex nextforlist) {
        this.nextforlist = nextforlist;
    }
}
