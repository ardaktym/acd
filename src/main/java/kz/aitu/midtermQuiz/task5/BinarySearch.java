package kz.aitu.midtermQuiz.task5;

public class BinarySearch {
    public void search(int arr[], int n){
        if (n <= arr[arr.length-1]){
            sea(arr, 0, arr.length, n);
        } else {
            System.out.println("false");
        }
    }
    private void sea(int arr[], int low, int high, int b){
        int mid = (high+low)/2;
        if (arr[mid]==b){
            if (arr[mid+1]!=b){
                System.out.println(mid);
            } else {
                sea(arr,mid+1,high,b);
            }
        } else if (arr[mid]>b){
            sea(arr,low,mid-1,b);
        } else {
            sea(arr,mid+1,high,b);
        }
    }
}
