package kz.aitu.midtermQuiz.mid2;

public class Tree{
    Node root;
    public Tree(){
        this.root = root;
    }
    public void insert(Integer key) {
        Node newNode = new Node(key);
        if(root == null){
            root = newNode;
            return;
        }
        Node current = root;
        Node parent = null;
        while(true){
            parent = current;
            if(key < current.getData()){
                current = current.getLeft();
                if(current == null){
                    parent.left = newNode;
                    return;
                }
            }else {
                current = current.getRight();
                if(current == null){
                    parent.right = newNode;
                    return;
                }
            }
        }
    }
    public void printAllAscending(Node root) {
        if(root != null){
            printAllAscending(root.getLeft());
            System.out.print(" " + root.getData());
            printAllAscending(root.getRight());
        }
    }
    //task1
    public int heightA(Node root){
        if(root == null) return 1;
        else{
            int leftHeight = heightA(root.getLeft());
            int rightHeight = heightA(root.getRight());
            if(leftHeight > rightHeight){
                return leftHeight+1;
            }
            else{
                return rightHeight+1;
            }
        }
    }
    public void height(Node root){
        System.out.println(heightA(root));
    }


/*    public int totalSum(Node root){
        int sum = root.getData();
        if(root != null){
            totalSum(root.getLeft());
            System.out.println();
            totalSum(root.getRight());
        }
        sum = sum + root.getData();
        return sum;
    }*/


    public void sumTotal(Node root)
    {
        int sum = root.getData();
       sum = sum + root.getLeft().getData();
       sum = sum + root.getLeft().getRight().data;
       sum = sum + root.getLeft().getLeft().data;
       sum = sum + root.getRight().getRight().data;
    }


    //task3
    public void maxElement(Node root){
        int maximum = 0;
        if(root == null)
            return;
        else{
            while(root != null) {
                maximum = root.getData();
                root = root.getRight();
            }

        }
        System.out.println(maximum);
    }
}
