package kz.aitu.midtermQuiz.mid2;

public class Main1 {
    public static void main(String[] args) {
        Tree tree = new Tree();
        tree.insert(4);
        tree.insert(2);
        tree.insert(3);
        tree.insert(1);
        tree.insert(6);
        tree.insert(5);
        tree.insert(7);
        tree.insert(9);
        tree.insert(11);
        tree.insert(8);
        tree.printAllAscending(tree.root);
        System.out.println();
        tree.height(tree.root);
        tree.maxElement(tree.root);
        //tree.totalSum(tree.root);
        tree.sumTotal(tree.root);
    }
}
