package kz.aitu.week2;
import java.util.Scanner;
public class practice6 {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        char [][] a = new char[n][m];
        for (int i = 0; i < n; i ++) {
            for (int j = 0; j < m; j ++) {
                a[i][j] = scanner.next().charAt(0);
            }
        }
        if (matrixMaze(a,n,m,0,0))
            System.out.println("yes");
        System.out.println("no");
        }
    public static boolean matrixMaze(char [][] a,int i, int j,int n, int m) {
        if(a[n][m]=='#')
            return false;
        if(i==i-1 && j==i-1)
            return true;
        a[n][m] = '#';
        boolean flag = false;
        if(n+1 != i) flag = flag || matrixMaze(a,i+1,j,n,m);
        if(n != 0) flag = flag || matrixMaze(a,i-1,j,n,m);
        if(m+1 != j) flag = flag || matrixMaze(a,i,j+1,n,m);
        if(m != 0) flag = flag || matrixMaze(a,i+1,j,n,m);
        return flag;
    }
}
