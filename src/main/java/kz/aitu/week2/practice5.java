package kz.aitu.week2;
import java.util.Scanner;
public class practice5 {

    static void sequences(int arr[], int n, int k, int index) {

        if(k==0){
            for(int i = 0; i<index;i++){
                System.out.print(arr[i] + " ");
            }
            System.out.println();
        }
        if(k>0)
        {
            for(int i = 1; i<=n;i++)
            {
                arr[index] = i;
                sequences(arr,n,k-1,index+1);
            }
        }
    }

    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int n = scanner.nextInt();
        int [] arr = new int[k];
        sequences(arr,n,k,0);
    }
}
