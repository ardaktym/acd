package kz.aitu.week2;
import java.util.Scanner;
public class practice7 {

    static void permutationsOfString(String s,String s1){
        if(s.length() == 0){
            System.out.println(s1 + " ");
            return;
        }
        for(int i = 0; i < s.length(); i++)
        {
            char ch = s.charAt(i);
            String newS = s.substring(0,i) + s.substring(i+1);
            permutationsOfString(newS, s1 + ch);
        }
    }

    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        permutationsOfString(str,"");
    }
}
