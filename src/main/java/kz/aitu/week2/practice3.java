package kz.aitu.week2;

import java.util.Scanner;

public class practice3 {

    static void reverseString(int n)
    {
        if(n==0)
            return;
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        reverseString(n-1);
        System.out.println(str);
    }

    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        reverseString(n);
    }

}
