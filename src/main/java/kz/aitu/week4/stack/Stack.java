package kz.aitu.week4.stack;

import kz.aitu.week4.stack.Node;

public class Stack {
    Node top;
    public int top(){
        if(top == null)
            return 0;
        else
            return top.data;
    }
    public int pop(){
        Node pop1 = top;
        if(top == null)
            return 0;
        else{
            top = top.next;
            return pop1.data;
        }
    }
    public void push(int data) {
        Node newNode = new Node(data);
        if (top == null) top = newNode;
        else {
            top = newNode;
            top = top.next;
        }
    }

    public boolean empty() {
        if (top == null) return true;
        return false;
    }
    public int size() {
        Node current = top;
        int counter = 0;
        if (top == null) {
            return 0;
        } else {
            while (current != null) {
                current = current.next;
                counter++;
            }
        }
        return counter;
    }
}
