package kz.aitu.week4.stack;

public class Node {
    public int data;
    public Node next;

    public Node(int data){
        this.data = data;
    }

    public int data() {
        return data;
    }

    public void setData(int data) { this.data = data; }

    public Node next() {
        return next;
    }

    public void setNext(Node next) {
        this.next = null;
    }

}
