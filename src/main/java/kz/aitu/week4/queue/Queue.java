package kz.aitu.week4.queue;

public class Queue {
    Node head;
    Node tail ;

    public void add(){
        Node newNode = new Node();
        if(head == null){
            tail = newNode;
            head = newNode;
        }
        else{
            tail.next = newNode;
            newNode.next = null;
            tail = newNode;
        }
    }

    public int peek(){
        if(head == null)
            return 0;
        return head.data;
    }

    public int poll(){
        if(head == null)
            return 0;
        else{
            head = head.next;
            return head.data;
        }
    }

    public int size(){
        int counter = 0;
        Node currentNode = head;
        while(currentNode != null){
            currentNode = currentNode.next;
            counter ++;
        }
        return counter;
    }

}
