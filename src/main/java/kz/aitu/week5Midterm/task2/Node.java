package kz.aitu.week5Midterm.task2;

public class Node {
    public int data;
    public Node next;

    public Node(int data){
        this.data = data;
    }

    public int data() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node next() {
        return next;
    }

    public void setNext() {
        this.next = null;
    }
}
