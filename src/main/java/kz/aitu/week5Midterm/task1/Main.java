package kz.aitu.week5Midterm.task1;
public class Main {

    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        Node head = list.head();

        list.add(new Node("one"));
        list.add(new Node("two"));
        list.add(new Node("three"));
        list.add(new Node("four"));
        list.add(new Node("five"));

        list.push_back("six");

        Node newNode = list.head();
        list.print(newNode);
    }


}