package kz.aitu.week5Midterm.task1;
public class Node {
    public String data;
    public Node next;

    public void setData(String data) {
        this.data = data;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node(String data) {
        this.data = data;
    }

    public String data() {
        return data;
    }

    public Node next() {
        return next;
    }

    public String toString() {
        return this.data;
    }

    public String getData() {
        return data;
    }

    public Node getNext() {
        return next;
    }
}