package kz.aitu.week5Midterm.task3;

public class Group {
    private Node head;
    private Node tail;

    public void setHead(Node head) {
        this.head = head;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public Node getHead() {
        return head;
    }

    public Node getTail() {
        return tail;
    }

    public void push(Node block){
        if(head==null){
            head=block;
            tail=block;
        }
        else {
            tail.setNext(block);
            tail=block;
        }
    }

    public Node pop(){
        head=head.getNext();
        return head;
    }

    public int printGroup(Group group) {//O(n)
        Node node = group.getHead();
        while (node != null) {
            if(node.getData().length()%2!=0){
                System.out.print(node.getData() + " ");
            }
            node = node.getNext();
        }
        System.out.println();
        return 0;
    }

}
