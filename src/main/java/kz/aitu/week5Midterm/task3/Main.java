package kz.aitu.week5Midterm.task3;

public class Main {
    public static void main(String[] args) {
        Group group = new Group();
        group.push(new Node("one"));
        group.push(new Node("two"));
        group.push(new Node("three"));
        group.push(new Node("four"));
        group.push(new Node("five"));
        group.pop();
        group.printGroup(group);
    }

}
